package com.example.siminformation

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.telephony.SubscriptionManager
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.siminformation.ui.theme.SIMInformationTheme


@RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
class MainActivity : ComponentActivity() {

    var simInformation = ""

    @SuppressLint("MissingPermission")
    private fun getSIMInformation(): String {
        val subscriptionManager: SubscriptionManager =
            applicationContext.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager

        return subscriptionManager.activeSubscriptionInfoList.toString()
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                Toast.makeText(this, "Getting SIM Information", Toast.LENGTH_SHORT).show()
                simInformation = getSIMInformation()
            } else {
                Toast.makeText(this, "Information not available", Toast.LENGTH_SHORT).show()
            }
        }

    private fun requestPermissionsAndGetSimInfo() {
        requestPermissionLauncher.launch(
            Manifest.permission.READ_PHONE_STATE
        )
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissionsAndGetSimInfo()

        setContent {
            SIMInformationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    SIMInfo(simInformation)
                }
            }
        }
    }
}

@Composable
fun SIMInfo(info: String) {
    Text(text = info)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    SIMInformationTheme {
        SIMInfo("Android")
    }
}